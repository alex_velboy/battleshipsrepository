﻿using System;
namespace BattleShip.ShipsPlacing
{
    public interface IShipsPlacer
    {
        public FieldCellState[,] PutShipsInOrder(int size);
    }
}
