﻿using System;

namespace BattleShip.ShipsPlacing
{
    public class ConsoleShipsPlacer : IShipsPlacer
    {
        
        public FieldCellState[,] PutShipsInOrder(int size)
        {
            Console.WriteLine("enter the cell in the field");
            string[] coordinates = Console.ReadLine().Split(',');
            int x = Convert.ToInt32(coordinates[0]);
            int y = Convert.ToInt32(coordinates[1]);
            FieldCellState[,] array = new FieldCellState[size, size];
            array[x, y] = FieldCellState.Full;
            return array;
           
        }

       
    }
}
