﻿using System;
using BattleShip.CoordinatesGetting;
using BattleShip.ShipsPlacing;

namespace BattleShip
{

    public class Participant
    {
        private FieldCellState [,] playField;
        
        private readonly ICoordinatesGetter coordinatesGetter;

        private IShipsPlacer shipsPlacer;

        private event Action gameLost;

        int fieldSize;

        public Participant(int fieldSize, ICoordinatesGetter coordinatesGetter, IShipsPlacer shipsPlacer, Action gameLost = null)
        {
            this.coordinatesGetter = coordinatesGetter;
            playField = new FieldCellState[fieldSize, fieldSize];
            this.shipsPlacer = shipsPlacer;
            this.fieldSize = fieldSize;
            this.gameLost = gameLost;
        }

        public (int x, int y) GetShootCoordinates()
        {
            (int x, int y) = coordinatesGetter.GetCoordinatinates(fieldSize); //как 
            return (x, y);
        }

        public void PlaceShips()
        {
            // получить данные =
            playField = shipsPlacer.PutShipsInOrder(fieldSize);
        }

        public FieldCellState TakeDammage(int x, int y)
        {
            // 
            FieldCellState cellState = playField[x, y];

            if (cellState == FieldCellState.Full)
            {
                cellState = FieldCellState.Hitten;
            }

            playField[x, y] = FieldCellState.Empty;
            bool defeat = CheckDefeat();

            if (defeat)
            {
                gameLost?.Invoke();
            }

            return cellState ;
        }



        private bool CheckDefeat()
        {
            foreach (FieldCellState cell in playField)
            {
                if (cell == FieldCellState.Empty)
                {
                    return false;
                }
            }
            return true;
        }

        public void Shoot()
        {

        }

        public void GetEnemyLink()
        {

        }

        public void SetShotOnEnemyField(int shotX, int shotY, FieldCellState hit)
        {

        }

        //массив поля?
        // ссылка на противника - метод?
    }
}
