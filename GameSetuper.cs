﻿using System;
using BattleShip.CoordinatesGetting;
using BattleShip.ShipsPlacing;

namespace BattleShip
{

    public class GameSetuper
    {
        bool gameIsOver;
        
        public void RunGame()// поочередно раздает ходы
        {
            int fieldSize = GetGameFieldSize();

            ParticipantFactory factory = new ParticipantFactory();

            Participant player = factory.CreateParticipant<ConsoleCoordinatesGetter, ConsoleShipsPlacer>(fieldSize, () => ParticipantWon("The game is over. You've not won :("));
            Participant bot = factory.CreateParticipant<RandomCoordinatesGetter, RandomShipsPlacer>(fieldSize, () => ParticipantWon("The game is over. Congrats! You've won! ;)"));

            while (!gameIsOver)
            {
                Console.WriteLine("Players turn");
                SetParticipantTurn(player, bot);

                if (!gameIsOver)
                {
                    Console.WriteLine("bots turn");
                    SetParticipantTurn(bot, player);
                }

            }
        }

        private int GetGameFieldSize()
        {
            Console.WriteLine("set the size of the field");
            int fieldSize = Convert.ToInt32(Console.ReadLine());
            if (fieldSize <= 0)
            {
                throw new Exception(); //словить кеч
            }

            return fieldSize;
        }

        public void SetParticipantTurn(Participant participantShooter, Participant participantCatcher)
        {
            (int shotX, int shotY) = participantShooter.GetShootCoordinates();
            FieldCellState hit = participantCatcher.TakeDammage(shotX, shotY);
            participantShooter.SetShotOnEnemyField(shotX, shotY, hit);// vizualnaya sostavlyaushaya
            // вывести информацию в массиве 

        }

        private void ParticipantWon(string message)
        {
            gameIsOver = true;
            Console.WriteLine(message);
        }
    }
}
