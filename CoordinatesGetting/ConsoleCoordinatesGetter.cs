﻿using System;


namespace BattleShip.CoordinatesGetting
{
  
    public class ConsoleCoordinatesGetter : ICoordinatesGetter
    {
        
        public (int x, int y) GetCoordinatinates(int fieldSize)
        { 
            Console.WriteLine("set firstd coordinate ");
            int x = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("set second coordinate");
            int y = Convert.ToInt32(Console.ReadLine());

            return (x, y);
        }
    }
}
